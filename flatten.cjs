let flatArr = [];
function flatten(elements, depth){
    if(Array.isArray(elements)){
        if(typeof depth === 'undefined'){
            depth = 1;
        }
        let length = elements.length;
        for(let index = 0; index < length; index++){
            if(Array.isArray(elements[index]) && depth > 0){
                flatten(elements[index], depth - 1);
            }
            else{
                if(typeof elements[index] !== 'undefined'){
                    flatArr.push(elements[index]);
                }
            }
        }
    }
    return flatArr;
}
module.exports = flatten;