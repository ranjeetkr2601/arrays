function map(elements, iteratee){
    
    if(Array.isArray(elements) && typeof iteratee === 'function'){
        const mappedArr = [];
        let length = elements.length;
        for(let index = 0; index < length; index++){
            mappedArr.push(iteratee(elements[index], index, elements));
        }
        return mappedArr;
    }
    
}
module.exports = map;