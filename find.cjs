function find(elements, callBackFunc){
    let result;
    if(Array.isArray(elements) && typeof callBackFunc === 'function'){
        let length = elements.length;
        for(let index = 0; index < length; index++){
            if(callBackFunc(elements[index]) === true){
                result = true;
                break;
            }
        }
    }
    return result;
}
module.exports = find;