function reduce(elements, callBackFunc, startingValue){
    let result;
    if(Array.isArray(elements) && typeof callBackFunc === 'function'){
        let length = elements.length;
        result = startingValue;
        for(let index = 0; index < length; index++){
            if(index === 0 && typeof startingValue === 'undefined') {
                result = elements[0];
            }
            else{
                result = callBackFunc(result, elements[index], index, elements);
            }
        }
    }
    return result;
}
module.exports = reduce;