let reduce = require('../reduce.cjs')
let items = require('./testCases.cjs');
function callBackFunc(previousValue, currentValue){
    return previousValue + currentValue;
}

const result = reduce(items, callBackFunc);
console.log(result);