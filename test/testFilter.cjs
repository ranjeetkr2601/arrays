let filter = require('../filter.cjs');
const items = require('./testCases.cjs');

function callBackFunc(item){
    return item > 3;
}
const result = filter(items, callBackFunc);
console.log(result);