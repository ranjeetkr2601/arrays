let map = require('../map.cjs');
const items = require('./testCases.cjs');

function iteratee(value){
    return value *= 2;
}
const result = map(items, iteratee);

console.log(result);