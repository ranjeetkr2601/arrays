let each = require('../each.cjs');
const items = require('./testCases.cjs');

function callBackFunc(item, index){
    console.log(('items[' + index + '] = ' + item));
}
each(items, callBackFunc);