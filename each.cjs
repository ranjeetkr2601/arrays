function each(elements, callBackFunc){
    if(Array.isArray(elements) && typeof callBackFunc === 'function'){
        let length = elements.length;
        for(let index = 0; index < length; index++){
            callBackFunc(elements[index], index);
        }
    }
}
module.exports = each;