function filter(elements, callBackFunc){
    let result = [];
    if(Array.isArray(elements) && typeof callBackFunc === 'function'){
        let length = elements.length;
        for(let index = 0; index < length; index++){
            if(callBackFunc(elements[index], index, elements) === true){
                result.push(elements[index]);
            }
        }
    }
    return result;
}
module.exports = filter;